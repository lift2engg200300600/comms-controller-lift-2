#include <Wire.h>
#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>
Adafruit_MMA8451 mma = Adafruit_MMA8451();

//hallEffectSensor Definition
#define hallPin1 37
#define hallPin2 38
#define hallPin3 39
/*
#define hallPin4 5
#define hallPin5 6
*/

//Accelerometer & LoadCell Definition

#define accelerometer 7
#define loadSensor 20
//#define loadSensor 8


//Motor Pin Definition
#define motorPin 2

//integer storage for sensor values
int hallValue1;
int hallValue2;
int hallValue3;
int hallValue4;
int hallValue5;
int loadValue;
int accelValue;
int motorCount;
int motorRev;

void setup() {
  Serial.begin(9600);

  //Initialize Hall Efffect Snesors
  pinMode(hallPin1,INPUT);
  pinMode(hallPin2,INPUT);
  pinMode(hallPin3,INPUT);

  // Interrupt to Count Encoder Pulses
  attachInterrupt(motorPin,motorRevolutions,FALLING);
}

void loop() {

}


void sensorRead(int sensorCheck){
  //case switches when different sensor needs to be checked (floor dependent)
  switch(sensorCheck){
    
    case hallPin1:
      hallValue1 = analogRead(hallPin1);
      Serial.println(hallValue1);
      break;
    
    case hallPin2:
      hallValue2 = analogRead(hallPin2);
      Serial.println(hallValue2);
      break;
    
    case hallPin3:
      hallValue3 = analogRead(hallPin3);
      Serial.println(hallValue3);
      break;
    /*
    case hallPin4:
      hallValue4 = analogRead(hallPin4);
      Serial.println(hallValue4);
      break;
    
    case hallPin5:
      hallValue5 = analogRead(hallPin5);
      Serial.println(hallValue5);
      break;
    */

  }
 
}

void accelCheck(){
    mma.read();
  
    // Get a new sensor event
    sensors_event_t event; 
    mma.getEvent(&event);
    Serial.print("Y: \t"); Serial.print(event.acceleration.y); Serial.print("\t");
    Serial.println("m/s^2 ");
    accelValue = event.acceleration.y;

}

void weightCheck(){
  loadValue = analogRead (loadSensor);
  Serial.println(loadValue);
}
// Function to count Revolutions of Motor Using Encoder Pulses
 void motorRevolutions(){
  motorCount++;
  //Convorts 7 Pulses Into 1 Revolution According to Specs
  if(motorCount % 7 == 0){
    motorRev++;
  }
 }
